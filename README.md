# Otus.Teaching.PromoCodeFactory

Проект для домашних заданий и демо по курсу `C# ASP.NET Core Разработчик` от `Отус`.
Cистема `Promocode Factory` для выдачи промокодов партнеров для клиентов по группам предпочтений.

Подробное описание проекта и описание домашних заданий можно найти в [Wiki](https://gitlab.com/devgrav/otus.teaching.promocodefactory/-/wikis/Home)


## Описание/Пошаговая инструкция выполнения домашнего задания:   

1. Добавляем RabbitMq в docker-compose файл.    
2. Подключаем RabbitMq в микросервисы Выдача промокода клиенту (Otus.Teaching.Pcf.GivingToCustomer), Администрирование (Otus.Teaching.Pcf.Administration) и по желанию в Получение промокода от партнера (Otus.Teaching.Pcf.ReceivingFromPartner). Можно использовать драйвер RabbitMq для .NET, тогда вероятно нужно будет использовать Hosted Service для прослушивания очереди, также можно использовать MassTransit или NServiceBus.    
3. При получении промокода от партнера в микросервисе Otus.Teaching.Pcf.ReceivingFromPartner в методе ReceivePromoCodeFromPartnerWithPreferenceAsync контроллера PartnersController в конце выполнения сейчас происходят вызовы _givingPromoCodeToCustomerGateway и _administrationGateway, где через HTTPClient изменяются данных в других микросервисах, такой способ является синхронным и при росте нагрузке или отказе одного из сервисов приведет к отказу всей операции.    
4. Для повышения надежности вместо синхронных вызовов нужно отправлять одно событие в RabbitMq, тогда в микросервисах  Otus.Teaching.Pcf.Administration и по желанию в Otus.Teaching.Pcf.GivingToCustomer нужно сделать подписку на данное событие и реализовать аналог текущих синхронных операций, вызываемых через API.    
5. Для того, чтобы ту же логику можно было вызвать из консюмера очереди ее надо перенести из контроллера в класс-сервис, который нужно разместить в проекте Core, в контроллере и консюмере очереди надо вызывать метод этого сервиса.    
