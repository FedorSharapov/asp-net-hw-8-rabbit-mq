﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.BusinessLogic.Abstractions;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IPromocodesService _promocodesService;

        public PromocodesController(IPromocodesService promocodesService)
        {
            _promocodesService = promocodesService;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promocodesService.GetAllAsync();

            var promocodesResponce = promocodes.Select(x => new PromoCodeShortResponse()
            {
                Id = x.Id,
                Code = x.Code,
                BeginDate = x.BeginDate.ToString("yyyy-MM-dd"),
                EndDate = x.EndDate.ToString("yyyy-MM-dd"),
                PartnerId = x.PartnerId,
                ServiceInfo = x.ServiceInfo
            }).ToList();

            return Ok(promocodesResponce);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            await _promocodesService.GivePromoCodesToCustomersWithPreferenceAsync(
                new BusinessLogic.Dto.GivePromoCodeToCustomerDto
                {
                    ServiceInfo = request.ServiceInfo,
                    PartnerId = request.PartnerId,
                    PromoCodeId = request.PromoCodeId,
                    PromoCode = request.PromoCode,
                    PreferenceId = request.PreferenceId,
                    BeginDate = request.BeginDate,
                    EndDate = request.EndDate
                });

            return CreatedAtAction(nameof(GetPromocodesAsync), new { }, null);
        }
    }
}