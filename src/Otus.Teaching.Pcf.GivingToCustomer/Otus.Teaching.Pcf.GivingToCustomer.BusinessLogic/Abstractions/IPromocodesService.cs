﻿using Otus.Teaching.Pcf.GivingToCustomer.BusinessLogic.Dto;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.BusinessLogic.Abstractions
{
    public interface IPromocodesService
    {
        Task<IEnumerable<PromoCode>> GetAllAsync();
        Task GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeToCustomerDto request);
    }
}
