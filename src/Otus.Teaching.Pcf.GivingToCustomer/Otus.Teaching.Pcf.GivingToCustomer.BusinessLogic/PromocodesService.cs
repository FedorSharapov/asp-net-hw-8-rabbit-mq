﻿using Otus.Teaching.Pcf.GivingToCustomer.BusinessLogic.Abstractions;
using Otus.Teaching.Pcf.GivingToCustomer.BusinessLogic.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.BusinessLogic.Dto;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services
{
    public class PromocodesService : IPromocodesService
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;

        public PromocodesService(IRepository<PromoCode> promoCodesRepository,
            IRepository<Preference> preferencesRepository, IRepository<Customer> customersRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
        }

        public async Task<IEnumerable<PromoCode>> GetAllAsync()
        {
            return await _promoCodesRepository.GetAllAsync();
        }

        public async Task GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeToCustomerDto dto)
        {
            //Получаем предпочтение по имени
            var preference = await _preferencesRepository.GetByIdAsync(dto.PreferenceId);

            if (preference == null)
                throw new System.Exception("BadRequest");

            //  Получаем клиентов с этим предпочтением:
            var customers = await _customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            PromoCode promoCode = PromoCodeMapper.MapFromModel(dto, preference, customers);

            await _promoCodesRepository.AddAsync(promoCode);
        }
    }
}
