﻿using Otus.Teaching.Pcf.Common;
using Otus.Teaching.Pcf.Common.Abstractions;
using Otus.Teaching.Pcf.GivingToCustomer.BusinessLogic.Abstractions;
using Otus.Teaching.Pcf.GivingToCustomer.BusinessLogic.Dto;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration.EventHandlers
{
    public class GivePromoCodesToCustomersWithPreferenceEventHandler : IIntegrationEventHandler<GivePromoCodeToCustomerDto>
    {
        private readonly IPromocodesService _promocodesService;

        public GivePromoCodesToCustomersWithPreferenceEventHandler(IPromocodesService promocodesService)
        {
            _promocodesService = promocodesService;
        }

        public async Task Handle(IntegrationContext<GivePromoCodeToCustomerDto> msgContext)
        {
            var msg = msgContext.Message;
            await _promocodesService.GivePromoCodesToCustomersWithPreferenceAsync(msg);
        }
    }
}
