﻿using System;

namespace Otus.Teaching.Pcf.Administration.Integration.Dto
{
    public class NotifyAdminAboutPartnerManagerPromoCodeDto
    {
        public Guid PartnerManagerId { get; set; }
    }
}
