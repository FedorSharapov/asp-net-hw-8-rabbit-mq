﻿using Otus.Teaching.Pcf.Administration.BusinessLogic.Abstractions;
using Otus.Teaching.Pcf.Administration.Integration.Dto;
using Otus.Teaching.Pcf.Common;
using Otus.Teaching.Pcf.Common.Abstractions;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Integration.EventHandlers
{
    public class AppliedPromocodesEventHandler : IIntegrationEventHandler<NotifyAdminAboutPartnerManagerPromoCodeDto>
    {
        private readonly IEmployeesService _employeesService;

        public AppliedPromocodesEventHandler(IEmployeesService employeesService)
        {
            _employeesService = employeesService;
        }

        public async Task Handle(IntegrationContext<NotifyAdminAboutPartnerManagerPromoCodeDto> msgContext)
        {
            var msg = msgContext.Message;
            await _employeesService.UpdateAppliedPromocodesAsync(msg.PartnerManagerId);
        }
    }
}
