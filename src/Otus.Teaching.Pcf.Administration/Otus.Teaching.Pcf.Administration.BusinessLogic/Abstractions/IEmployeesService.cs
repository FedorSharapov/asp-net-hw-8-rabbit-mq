﻿using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.BusinessLogic.Abstractions
{
    public interface IEmployeesService
    {
        public Task<IEnumerable<Employee>> GetAllAsync();

        public Task<Employee> GetByIdAsync(Guid id);

        public Task UpdateAppliedPromocodesAsync(Guid id);
    }
}
