﻿using Otus.Teaching.Pcf.Common.Abstractions;
using System.Collections.Generic;

namespace Otus.Teaching.Pcf.Common
{
    public class IntegrationContext<T> where T : class
    {
        public string CorrelationId { get; internal set; }
        public T Message { get; internal set; }
        public string ExchangeName { get; internal set; }
        public string RoutingKey { get; internal set; }
        public IDictionary<string, object> Headers { get; internal set; }
    }
}