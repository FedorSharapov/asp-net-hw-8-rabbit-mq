﻿using System;

namespace Otus.Teaching.Pcf.Common.Abstractions
{
    public interface IEventBus : IDisposable
    {
        void Subscribe<T, TH>(string routingKey = null)
            where T : class
            where TH : IIntegrationEventHandler<T>;

        void Publish<T>(T @event, string routingKey = null);
    }
}