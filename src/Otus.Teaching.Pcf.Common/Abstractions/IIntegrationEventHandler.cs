﻿using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Common.Abstractions
{
    public interface IIntegrationEventHandler<T> where T : class
    {
        Task Handle(IntegrationContext<T> @event);
    }
}
