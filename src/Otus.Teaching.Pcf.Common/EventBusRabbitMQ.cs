﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Pcf.Common.Abstractions;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Text.Json;

namespace Otus.Teaching.Pcf.Common
{
    public class EventBusRabbitMQ : IEventBus
    {
        private readonly IServiceProvider _provider;
        private readonly IConnection _connection;
        private readonly IModel _channel;

        private string _exchangeName;
        private bool _disposed;

        public EventBusRabbitMQ(IServiceProvider provider, IConfiguration configuration)
        {
            _provider = provider;

            var connectionFactory = new ConnectionFactory
            {
                ClientProvidedName = configuration["RabbitMQOptions:ClientProvidedName"],
                HostName = configuration["RabbitMQOptions:HostName"],
                VirtualHost = configuration["RabbitMQOptions:VirtualHost"],
                UserName = configuration["RabbitMQOptions:UserName"],
                Password = configuration["RabbitMQOptions:Password"],
                DispatchConsumersAsync = true
            };

            _exchangeName = configuration["RabbitMQOptions:ExchangeName"];

            _connection = connectionFactory.CreateConnection();
            _channel = _connection.CreateModel();
        }

        public void Publish<T>(T @event, string routingKey = null)
        {
            routingKey ??= typeof(T).Name;

            ExchangeDeclare();

            var jsonMessage = JsonSerializer.Serialize(@event);
            var body = Encoding.UTF8.GetBytes(jsonMessage);

            var properties = _channel.CreateBasicProperties();
            properties.CorrelationId = Guid.NewGuid().ToString();

            _channel.BasicPublish(
                exchange: _exchangeName,
                routingKey: routingKey,
                basicProperties: properties,
                body: body);
        }

        public void Subscribe<T, TH>(string queueName = null)
            where T : class
            where TH : IIntegrationEventHandler<T>
        {
            queueName ??= typeof(T).Name;

            ExchangeDeclare();
            QueueBind(queueName);

            var consumer = new AsyncEventingBasicConsumer(_channel);
            consumer.Received += async (obj, args) =>
            {
                using (var scope = _provider.CreateScope())
                {
                    var handler = scope.ServiceProvider.GetRequiredService<IIntegrationEventHandler<T>>();

                    var headers = args.BasicProperties.Headers;
                    var messageJson = Encoding.UTF8.GetString(args.Body.ToArray());
                    var message = JsonSerializer.Deserialize<T>(messageJson);
                    var routingKey = args.RoutingKey;
                    var context = new IntegrationContext<T>
                    {
                        CorrelationId = args.BasicProperties.CorrelationId,
                        ExchangeName = _exchangeName,
                        RoutingKey = routingKey,
                        Message = message,
                        Headers = args.BasicProperties.Headers
                    };
                    await handler.Handle(context);

                    _channel.BasicAck(args.DeliveryTag, false);
                }
            };

            _channel.BasicConsume(queueName, false, consumer);
        }

        private void ExchangeDeclare()
        {
            if (_exchangeName != null)
                _channel.ExchangeDeclare(_exchangeName, ExchangeType.Direct, false);
        }

        private void QueueBind(string queueName)
        {
            _channel.QueueDeclare(queueName, false, false, false);

            if (_exchangeName != null)
                _channel.QueueBind(queueName, _exchangeName, queueName);
        }

        public void Dispose()
        {
            if (_disposed)
                return;

            _disposed = true;
            _channel?.Dispose();
            _connection?.Dispose();

            GC.SuppressFinalize(this);
        }
    }
}
