﻿using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto
{
    public class NotifyAdminAboutPartnerManagerPromoCodeDto
    {
        public Guid PartnerManagerId { get; set; }
    }
}
