﻿using Otus.Teaching.Pcf.Common.Abstractions;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Producers
{
    public class GivingPromoCodeToCustomerProducer : IGivingPromoCodeToCustomerGateway
    {
        private readonly IEventBus _eventBus;

        public GivingPromoCodeToCustomerProducer(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }

        public Task GivePromoCodeToCustomer(PromoCode promoCode)
        {
            var dto = new GivePromoCodeToCustomerDto()
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PromoCode = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
            };

            _eventBus.Publish(dto);

            return Task.CompletedTask;
        }
    }
}
