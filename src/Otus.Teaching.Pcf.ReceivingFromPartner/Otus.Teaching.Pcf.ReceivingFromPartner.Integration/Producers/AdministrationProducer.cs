﻿using Otus.Teaching.Pcf.Common.Abstractions;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Producers
{
    public class AdministrationProducer : IAdministrationGateway
    {
        private readonly IEventBus _eventBus;

        public AdministrationProducer(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }

        public Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId)
        {
            var dto = new NotifyAdminAboutPartnerManagerPromoCodeDto()
            {
                PartnerManagerId = partnerManagerId
            };

            _eventBus.Publish(dto);

            return Task.CompletedTask;
        }
    }
}